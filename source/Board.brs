Function CreateBoard(theMaze As Object,theDirections As Object) As Object
    theBoard = CreateObject("roList")
    For Each theColumn In theMaze
        theBoardColumn = CreateObject("roList")
        theBoard.Push(theBoardColumn)
        For Each theCell in theColumn
            theBoardCell = CreateBoardCell(theCell,theDirections)
            theBoardColumn.Push(theBoardCell)
        End For
    End For
    For theColumn=0 to theBoard.Count()-1
        For theRow = 0 to theBoard[theColumn].Count()-1
            theCell = theBoard[theColumn][theRow]
            For Each theDirection in theDirections
                theNextColumn = theDirection.GetNextX(theColumn,theRow)
                theNextRow = theDirection.GetNextY(theColumn,theRow)
                if theNextColumn>=0 And theNextRow>=0 And theNextColumn<theBoard.Count() And theNextRow<theBoard[theColumn].Count() Then
                    theNextCell = theBoard[theNextColumn][theNextRow]
                    theCell.neighbors[theDirection.ordinal]=theNextCell
                End If
            End For
        End For
    End For
    theBoard.Clear = Function ()
            For Each theColumn in m
                For Each theCell in theColumn
                    theCell.clear()
                End For
            End For
        End Function
    Return theBoard    
End Function
Function CreateBoardCell(theMazeCell As Object,theDirections As Object) As Object
    theCell = {}
    theCell.neighbors = CreateObject("roArray",theDirections.Count(),false)
    theCell.connections = CreateObject("roArray",theDirections.Count(),false)
    theCell.isSource=false
    theCell.isLit = false
    For Each theDirection In TheDirections
        theCell.connections[theDirection.ordinal]=(theMazeCell.connections[theDirection.ordinal]<>invalid And theMazeCell.connections[theDirection.ordinal].open)
    End For
    theCell.GetValue = Function()
            theValue = 0
            For theIndex=0 To m.connections.Count()-1
                If m.connections[theIndex] Then
                    theValue = theValue + 2 ^ theIndex
                End If   
            End For
            Return theValue
        End Function
    theCell.RotateCCW = Function()
            theTemp = m.connections[0]
            For theIndex=0 To m.connection.Count()-2
                m.connections[theIndex]=m.connection[theIndex+1]
            End For
            m.connections[m.connections.Count()-1]=theTemp
        End Function
    theCell.RotateCW = Function()
            theTemp = m.connections[m.connections.Count()-1]
            For theIndex=m.connections.Count()-1 To 1 Step -1
                m.connections[theIndex]=m.connection[theIndex-1]
            End For
            m.connections[0]=theTemp
        End Function
    theCell.Clear = Function():End Function
    Return theCell
End Function