Function CreateGame()
    theGame = CreateObject("roAssociativeArray")
    theGame.screen = CreateObject("roScreen")
    theGame.messagePort = CreateObject("roMessagePort")
    theGame.screen.SetAlphaEnable(true)
    theGame.screen.SetMessagePort(theGame.messagePort)
    theGame.imageManager = CreateImageManager(theGame)
    theGame.gameStates = CreateObject("roAssociativeArray")
    theGame.gameStates["titlescreen"] = CreateTitleScreenState(theGame)
    theGame.gameStates["aboutscreen"] = CreateAboutScreenState(theGame)
    theGame.gameStates["instructionsscreen"] = CreateInstructionsScreenState(theGame)
    theGame.gameStates["playscreen"] = CreatePlayScreenState(theGame)
    theGame.currentState="titlescreen"
    theGame.Run = Function()
            While True
                theGame.gameStates[theGame.currentState].Run()
            End While
        End Function
    Return theGame
End Function
Function CreateTitleScreenState(theGame)
    theState = CreateObject("roAssociativeArray")
    theState.game = theGame
    theState.Run = Function()
            theItem = 0
            redraw = true
            theScreen = m.game.screen
            While True
                If redraw Then
                    theScreen.Clear(&HFFFFFFFF)
                    'TODO: draw stuff
                    'draw background
                    'draw "play" menu item - option = 0
                    'draw "intructions" menu item - option = 1
                    'draw "about" menu item - option = 2 
                    theScreen.SwapBuffers()
                    redraw = false
                End If
                theEvent = Wait(0,theScreen.GetMessagePort())
                If Type(theEvent)="roUnivrsalControlEvent" Then
                    If theEvent.GetInt()=2 Then'up
                        If theItem>0 Then
                            redraw = true
                            theItem=theItem-1
                        End If
                    Else If theEvent.GetInt()=3 Then'down
                        If theItem<2 Then
                            redraw = true
                            theItem=theItem+1
                        End If
                    Else If theEvent.GetInt()=6 Then'select
                        If theItem=0 Then
                            theGame.currentState="playscreen"
                        Else If theItem=1 Then
                            theGame.currentState="instructionsscreen"
                        Else If theItem=2 Then
                            theGame.currentState="aboutscreen"
                        End If
                        Return
                    End If
                End If
            End While
        End Function
    Return theState
End Function
Function CreateAboutScreenState(theGame)
    theState = CreateObject("roAssociativeArray")
    theState.game = theGame
    theState.Run = Function()
        End Function
    Return theState
End Function
Function CreateInstructionsScreenState(theGame)
    theState = CreateObject("roAssociativeArray")
    theState.game = theGame
    theState.Run = Function()
        End Function
    Return theState
End Function
Function CreatePlayScreenState(theGame)
    theState = CreateObject("roAssociativeArray")
    theState.game = theGame
    theState.Run = Function()
        End Function
    Return theState
End Function
Function CreateImageManager(theGame)
    theManager = CreateObject("roAssociativeArray")
    theManager.game = theGame
    theManager.images = CreateObject("roAssociativeArray")
    'HD
    theManager.images["background"]=CreateObject("roBitmap","pkg:/locale/default/images/hd/background.png")
    theManager.images["red"]=CreateObject("roBitmap","pkg:/locale/default/images/hd/red.png")
    theManager.images["green"]=CreateObject("roBitmap","pkg:/locale/default/images/hd/green.png")
    theManager.images["yellow"]=CreateObject("roBitmap","pkg:/locale/default/images/hd/yellow.png")
    theManager.spriteSets = CreateObject("roAssociativeArray")
    theManager.spriteSets["red"] = CreateObject("roArray",16,false)
    theManager.spriteSets["green"] = CreateObject("roArray",16,false)
    theManager.spriteSets["yellow"] = CreateObject("roArray",16,false)
    theColumnWidth = 80
    theRowHeight = 80
    For theColumn = 0 To 3
        For theRow = 0 To 3
            theIndex = theColumn + theRow * 4
            theManager.spriteSets["red"][theIndex]=CreateObject("roRegion",theManager.images["red"],theColumn*theColumnWidth,theRow*theRowHeight,theColumnWidth,theRowHeight)
            theManager.spriteSets["green"][theIndex]=CreateObject("roRegion",theManager.images["green"],theColumn*theColumnWidth,theRow*theRowHeight,theColumnWidth,theRowHeight)
            theManager.spriteSets["yellow"][theIndex]=CreateObject("roRegion",theManager.images["yellow"],theColumn*theColumnWidth,theRow*theRowHeight,theColumnWidth,theRowHeight)
        End For
    End For
    Return theManager
End Function
Function CreatePlotter(theGame)
    thePlotter = CreateObject("roAssociativeArray")
    'HD
    thePlotter.boardX = 320
    thePlotter.boardY = 40
    thePlotter.boardCellWidth = 80
    thePlotter.boardCellHeight = 80
    thePlotter.PlotX = Function(theColumn,theRow)
        End Function
    return thePlotter
End Function