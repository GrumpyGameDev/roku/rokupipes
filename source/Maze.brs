Function CreateMaze(theColumns As Integer,theRows As Integer,theDirections As Object) As Object
    theMaze = CreateObject("roArray",theColumns,false)
    For theColumn = 0 To theColumns-1
        theMaze[theColumn] = CreateObject("roArray",theRows,false)
        For theRow = 0 To theRows-1
            theMaze[theColumn][theRow] = CreateMazeCell(theDirections.Count())
        End For
    End For
    For theColumn = 0 To theColumns-1
        For theRow = 0 To theRows-1
            theCell = theMaze[theColumn][theRow]
            For Each theDirection In theDirections
                If (theCell.neighbors[theDirection.ordinal]=invalid) Then
                    theNextColumn = theDirection.GetNextX(theColumn,theRow)
                    theNextRow = theDirection.GetNextY(theColumn,theRow)
                    If(theNextColumn>=0 And theNextRow>=0 And theNextColumn<theColumns And theNextRow<theRow) Then
                        theNextCell = theMaze[theNextColumn][theNextRow]
                        theCell.neighbors[theDirection.ordinal] = theNextCell
                        theNextCell.neighbors[theDirection.opposite] = theCell
                        theConnection = CreateMazeConnection()
                        theCell.connection[theDirection.ordinal] = theConnection
                        theNextCell.connection[theDirection.opposite] = theConnection
                    End If
                End If
            End For
        End For
    End For
    theFrontier = CreateObject("roList")
    theColumn = Rnd(theColumns)-1
    theRow = Rnd(theRow)-1
    theCell = theMaze[theColumn][theRow] 
    theCell.value=1
    For Each theDirection In theDirections
        theNextCell = theCell.neighbors[theDirection.ordinal] 
        If theNextCell<>invalid Then
            theNextCell.value=0
            theFrontier.Push(theNextCell)
        End If
    End For
    theCandidates = CreateObject("roList")
    While theFrontier.Count()>0
        theIndex = Rnd(theFrontier.Count()-1)
        theCell = theFrontier[theIndex]
        If theIndex<theFrontier.Count()-1 Then
            theFrontier[theIndex]=theFrontier.Pop()
        Else
            theFrontier.Pop()
        End If
        theCell.value=1
        theCandidates.Clear()
        For Each theDirection In TheDirections
            theNextCell = theCell.neighbors[theDirection.ordinal] 
            If theNextCell<>invalid Then
                If theNextCell.value=-1 Then
                    theNextCell.value=0
                    theFrontier.Push(theNextCell)
                Else If theNextCell.value=1 Then
                    theCandidates.Push(theDirection)
                End If
            End If
        End For
        theDirection = theCandidates[Rnd(theCandidates.Count())-1]
        theCell.connections[theDirection.ordinal].open = true
    End While
    Return theMaze
End Function
Function CreateMazeCell(theDirectionCount As Integer) As Object
    theCell = {}
    theCell.neighbors = CreateObject("roArray",theDirectionCount,false)
    theCell.connections = CreateObject("roArray",theDirectionCount,false)
    theCell.value = -1
    Return theCell
End Function
Function CreateMazeConnection() As Object
    theConnection = {}
    theConnection.open = false
    return theConnection
End Function