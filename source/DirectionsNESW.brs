Function CreateDirectionsRectangular4() As Object
    theDirections = CreateObject("roArray",4,false)
    theDirections[0] = CreateDirection(0,1,2,3,Function(x,y):Return x:End Function,Function(x,y):Return y-1:End Function)
    theDirections[1] = CreateDirection(1,2,3,0,Function(x,y):Return x+1:End Function,Function(x,y):Return y:End Function)
    theDirections[2] = CreateDirection(2,3,0,1,Function(x,y):Return x:End Function,Function(x,y):Return y+1:End Function)
    theDirections[3] = CreateDirection(3,0,1,2,Function(x,y):Return x-1:End Function,Function(x,y):Return y:End Function)
    return theDirections
End Function
Function CreateDirection(theOrdinal,theNext,theOpposite,thePrevious,theNextXFunction,theNextYFunction) As Object
    theDirection = {}
    theDirection.ordinal = theOrdinal
    theDirection.next = theNext
    theDirection.opposite = theOpposite
    theDirection.previous = thePrevious
    theDirection.GetNextX = theNextXFunction
    theDirection.GetNextY = theNextYFunction
    return theDirection
End Function